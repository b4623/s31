const express = require('express');

const mongoose = require("mongoose");

const taskRoute= require("./routes/taskroutes");

const app = express();

const port = 3001;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://mikemike:mikemike@cluster0.xb2ky.mongodb.net/batch164_to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on ("error", console.error.bind(console, 'connection error'));

db.once("open", () => console.log("were connected in cloud database"))

app.use("/tasks", taskRoute);







app.listen(port, ()=> console.log(`Server running at port:${port}`));
