const express = require("express");
const router = express.Router();

const TaskController = require('../controllers/taskController');


router.get("/", (req, res) => {
	TaskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})


router.post("/", (req, res) => {
	TaskController.createTask(req.body).then(result => res.send(result));
})

router.delete("/:taskId", (req, res)=>{
	TaskController.deleteTask(req.params.taskId).then(result => res.send(result));
})

router.put("/:id", (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

router.get("/:id", (req, res) => {
	TaskController.getSpecificTask(req.params.id).then(result => res.send(result));	
})

router.put('/:id/complete', (req, res)=>{
	TaskController.completeTask(req.params.id).then(result => res.send(result));	
})

module.exports = router;
